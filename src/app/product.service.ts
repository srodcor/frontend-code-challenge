import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Product } from './product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private productsUrl = 'http://localhost:4200/assets/data.json';

  constructor(private http: HttpClient) { }

  getProducts() {
    return this.http.get<Product>(this.productsUrl);
  } 
}


