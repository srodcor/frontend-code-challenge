export class Product {
  "best-sellers": object;
  "releases": object;
  // title: string;
  // price: number;
  // installments: object;
  // "free-shipping": boolean;
  // image: string;
}

// <div class="row">
//         <div class="col-md-3" *ngFor="let product of products.bestsellers">
//           <div class="card">
//               <img class="card-img-top" src="{{product.image}}" alt="Card image cap">
//             <div class="card-body text-center">
//               <!-- <h3 class="card-title">Nodebook 2 em 1 <br> Acer - Windows 10</h3> -->
//               <h3 class="card-title">{{product.title}}</h3>
//               <p class="card-text">
//                 <strong>R$ {{product.price}}</strong> <br> <span>{{product.installments.number}}x de R$ {{product.installments.value}}</span> <span class="card-free">Frete Grátis</span>
//               </p>
//               <a href="#" class="btn btn-success btn-block">Comprar</a>
//             </div>
//           </div>
//         </div>
//       </div>