import { Component } from '@angular/core';
import { ProductService } from './product.service';
import { Product } from './product';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  products = {};
  title = 'app';

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.getProducts(); 
  }

  getProducts() {
    this.productService.getProducts()
    .subscribe(product => this.products = {
      bestsellers: product['best-sellers'],
      releases: product['releases']
    });
  }
  
}
