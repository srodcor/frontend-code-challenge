$(function() {
    $('.flexslider').flexslider({
      animation: "slide",
      animationLoop: false,
      itemWidth: 271,
      itemMargin: 5
    });

    $(".mart-cart").mouseenter(function(){
      $(".mart-cart-item").addClass("animated swing");
    });

    $(".mart-search input").addClass("animated flipInX");
  });